#include <argp.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/select.h>
#include <stdio.h>
#include <signal.h>
#include <fcntl.h>

const char *argp_program_version = "0.0.1";
const char *argp_program_bug_address = "<seb@ici.fr>";
static char doc[] = "Simple program for a file rotate.";
static char args_doc[] = "[FILENAME]...";

/* The options we understand. */
static struct argp_option options[] = {
  {"name",  'n', "PIPE_NAME",       0, "Specify the namedpipe name (default /tmp/log_pipe)" },
  {"file-nb",    'b', "NB",       0, "Specify the number of files (default 10)" },
  {"file-size",    's', "SIZE",       0, "Specify the max size of files (default 100MB)" },
  {"file-template",   'o', "TEMP",  0, "Output file TEMP (default ./log_file_" },
  { 0 }
};
struct arguments {
  const char* pipe_name;
  const char* file_template;
  int file_nb;
  int file_size;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = state->input;
    switch (key) {
    case 'n': arguments->pipe_name = arg; break;
    case 'b': arguments->file_nb = atoi(arg); break;
    case 's': arguments->file_size = atoi(arg); break;
    case 'o': arguments->file_template = arg; break;
    case ARGP_KEY_ARG: return 0;
    default: return ARGP_ERR_UNKNOWN;
    }   
    return 0;
}

static void remove_log_file(const char* template,int file_id)
{
  char log_file_name[4096];
  snprintf(log_file_name,sizeof(log_file_name),
	   "%s_%d",template,file_id);
  unlink(log_file_name);
}

static FILE* open_log_file(const char* template,int file_id)
{
  char log_file_name[4096];
  snprintf(log_file_name,sizeof(log_file_name),
	   "%s_%d",template,file_id);
  
  FILE* log_file = fopen(log_file_name,"w");
  if(!log_file)
    printf("Can not open the file %s\n",log_file_name);
  return log_file;
}
int wakeup_pipe[2];
int read_pipe;
const char* namedpipe;

static void intHandler(int dummy) {
  close(wakeup_pipe[1]);
  unlink(namedpipe);
  if(!read_pipe)
    exit(0);
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc, 0, 0, 0 };
int main(int argc,char *argv[])
{
  struct arguments arguments;
  
  arguments.pipe_name = "/tmp/log_pipe";
  arguments.file_nb = 10;
  arguments.file_template= "./log_file";
  arguments.file_size = 100*1024*1024; /* 100MB */
  
  argp_parse(&argp, argc, argv, 0, 0, &arguments);
  namedpipe = arguments.pipe_name;
  unlink(arguments.pipe_name);
  if(mkfifo(arguments.pipe_name, 0666))
    {
      perror("Can't open the namedpipe");
      return -1;
    }


  if(pipe(wakeup_pipe))
    {
      printf("Can't open internal synchronization pipe");
      goto finally;
    }

  char buffer[64*1024];
  read_pipe = 0;
  
  int file_id = 0;
  int nb_bytes_to_write = arguments.file_size;
  FILE *log_fd = open_log_file(arguments.file_template,file_id);

  signal(SIGINT, intHandler);
  signal(SIGTERM, intHandler);
  

  if(!log_fd)
    goto finally;
  while(true)
    {
      int read_pipe = open(arguments.pipe_name,O_RDONLY);
      if(!read_pipe)
	break;
      
      while(read_pipe)
	{
	  fd_set rfds;
	  FD_ZERO(&rfds);
	  FD_SET(wakeup_pipe[0], &rfds);
	  FD_SET(read_pipe, &rfds);

	  int retval = select(read_pipe + 1, &rfds, NULL, NULL, NULL);
	  switch(retval)
	    {
	    case -1:
	      perror("An error occured on select");
	      goto finally;
	      break;
	    case 0:
	      //should never happen
	      break;
	    default:
	      if (FD_ISSET(wakeup_pipe[0], &rfds))
		goto finally;
	      else if(FD_ISSET(read_pipe,&rfds))
		{
		  ssize_t nb_bytes = read(read_pipe, buffer, sizeof(buffer));
		  if(!nb_bytes)
		    {
		      close(read_pipe);
		      read_pipe = 0;
		      continue;
		    }
		   	      
		  size_t nb_write_items = fwrite(buffer,1,nb_bytes,log_fd);
		  nb_bytes_to_write -= nb_write_items;
		  if(nb_bytes_to_write <= 0)
		    {
		      fclose(log_fd);
		      file_id += 1;
		      nb_bytes_to_write = arguments.file_size;
		      log_fd = open_log_file(arguments.file_template,file_id);
		      remove_log_file(arguments.file_template,file_id-arguments.file_nb);
		    }
		}
	      break;
	    }
	}
    }
 finally:
  unlink(arguments.pipe_name);
  fclose(log_fd);
  return 0;
}

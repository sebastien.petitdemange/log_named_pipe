# What is it
It's a simple tool to produce ring buffer file.

# Usage

Start the tool

```
log_named_pipe --file-template log_ls
```

By default it create a namedpipe `/tmp/log_pipe`

Then start the logger tool i.e:

```
strace -o /tmp/log_pipe ls -l
```

Then the `strace` command will be logged in **log_ls** files
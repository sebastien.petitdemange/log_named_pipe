all: log_named_pipe

log_named_pipe: log_named_pipe.c
	gcc -O2 -o log_named_pipe log_named_pipe.c

clean:
	rm -f log_named_pipe
